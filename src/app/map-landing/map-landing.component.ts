import { Component, OnInit } from '@angular/core';

import { LocationService } from '../core/services/location.service';

@Component({
  selector: 'app-map-landing',
  templateUrl: './map-landing.component.html',
  styleUrls: ['./map-landing.component.css']
})
export class MapLandingComponent implements OnInit {

  public lat = 51.678418;
  public lng = 7.809007;

  public map: any;

  public markerLat: number;
  public markerLng: number;

  public config = {
    minZoom: 8,
    disableDoubleClickZoom: true
  };

  public markerConfig = {
    iconUrl: 'assets/findme.png',
    title: 'Marker title'
  };

  constructor(
              private location: LocationService
            ) { }

  ngOnInit() {
  }


  setLocation(location) {
    this.lat = location.lat;
    this.lng = location.lng;
  }

  setZoom(zoom: number = 5) {
    this.map.setZoom(zoom);
  }

  mapReady(event) {
    this.map = event;
  }

  setMarker(event) {
    this.markerLat = event.coords.lat;
    this.markerLng = event.coords.lng;
  }

}
