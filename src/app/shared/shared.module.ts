import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GmapComponent } from './gmap/map.component';
import { AgmCoreModule } from '@agm/core';
import { environment } from '../../environments/environment';

@NgModule({
  imports: [
    CommonModule,
    AgmCoreModule.forRoot({
      apiKey: environment.mapsApiKey
    })
  ],
  declarations: [
    GmapComponent
  ],
  exports: [
    GmapComponent
  ]
})
export class SharedModule { }
