import { LatLngBounds, LatLngBoundsLiteral } from '@agm/core';
import { FullscreenControlOptions,
          MapTypeControlOptions,
          PanControlOptions,
          RotateControlOptions,
          ScaleControlOptions,
          StreetViewControlOptions,
          MapTypeStyle,
          ZoomControlOptions } from '@agm/core/services/google-maps-types';

export interface MapConfig {
  backgroundColor?: string;
  clickableIcons?: boolean;
  disableDefaultUI?: boolean;
  disableDoubleClickZoom?: boolean;
  draggableCursor?: string;
  draggingCursor?: string;
  fitBounds?: LatLngBoundsLiteral | LatLngBounds;
  fullscreenControl?: boolean;
  fullscreenControlOptions?: FullscreenControlOptions;
  gestureHandling?: 'cooperative' | 'greedy' | 'none' | 'auto' | string;
  keyboardShortcuts?: boolean;
  latitude?: number;
  longitude?: number;
  mapDraggable?: boolean;
  mapTypeControl?: boolean;
  mapTypeControlOptions?: MapTypeControlOptions;
  mapTypeId?: 'roadmap' | 'hybrid' | 'satellite' | 'terrain' | string;
  maxZoom?: number;
  minZoom?: number;
  panControl?: boolean;
  panControlOptions?: PanControlOptions;
  rotateControl?: boolean;
  rotateControlOptions?: RotateControlOptions;
  scaleControl?: boolean;
  scaleControlOptions?: ScaleControlOptions;
  scrollwheel?: boolean;
  streetViewControl?: boolean;
  streetViewControlOptions?: StreetViewControlOptions;
  styles?: MapTypeStyle[];
  usePanning?: boolean;
  zoom?: number;
  zoomControl?: boolean;
  zoomControlOptions?: ZoomControlOptions;
}
