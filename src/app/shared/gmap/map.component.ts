import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

import { MapConfig } from './map-config.interface';
import { LocationService } from '../../core/services/location.service';

declare var google: any;

interface MapMarker {
  latitude: string;
  longitude: string;
  label?: string;
  draggable?: boolean;
  icon?: string;
}

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class GmapComponent implements OnInit {

  public map: any;

  @Input() latitude: number;
  @Input() longitude: number;
  @Input() markerLat: number;
  @Input() markerLng: number;

  @Input() markerConfig: {};

  @Input() markerIcon: string;

  @Input() config: MapConfig;
  @Input() markers: MapMarker[];

  // AgmMap component events start
  @Output() mapClick = new EventEmitter();
  @Output() mapRightClick =  new EventEmitter();
  @Output() mapDblClick =  new EventEmitter();
  @Output() centerChange =  new EventEmitter();
  @Output() boundsChange =  new EventEmitter();
  @Output() idle =  new EventEmitter();
  @Output() zoomChange =  new EventEmitter();
  @Output() mapReady =  new EventEmitter();

  // AgmMarker components events start
  @Output() dragEnd = new EventEmitter();
  @Output() markerClick = new EventEmitter();
  @Output() mouseOut = new EventEmitter();
  @Output() mouseOver = new EventEmitter();

  // Default component config for AgmMap component
  readonly defaultConfig: MapConfig = {
    longitude: 0,
    latitude: 0,
    zoom: 8,
    disableDoubleClickZoom: false,
    disableDefaultUI: false,
    scrollwheel: true,
    keyboardShortcuts: true,
    zoomControl: true,
    styles: [],
    usePanning: false,
    streetViewControl: true,
    fitBounds: null,
    scaleControl: false,
    mapDraggable: true,
    mapTypeControl: false,
    panControl: false,
    rotateControl: false,
    fullscreenControl: false,
    mapTypeId: 'roadmap',
    clickableIcons: true,
    gestureHandling: 'auto',
  };

  constructor(private location: LocationService) {
   }

   ngOnInit() {
    this.config = Object.assign(this.defaultConfig, this.config);
   }

   onMapReady(event) {
     this.map = event;
     this.setupMap();
     this.mapReady.emit(event);
   }

   setupMap() {
    this.addControl(document.getElementById('btnLocateMe'), google.maps.ControlPosition.RIGHT_BOTTOM);
   }

   addControl(element: HTMLElement, position: any) {
    this.map.controls[position].push(element);
   }

   findMe() {
    this.location.getCurrentLocation().then(loc => {
      this.latitude = this.markerLat = loc.lat;
      this.longitude = this.markerLng = loc.lng;
      this.config.zoom = 18;
      this.map.setCenter(loc);
    });
  }

}
