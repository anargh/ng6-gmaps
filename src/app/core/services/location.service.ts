import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocationService {


  isLocationEnabled(): boolean {
    return navigator.geolocation ? true : false;
  }

  getCurrentLocation(): Promise<{ lat: any, lng: any }> {
    let currentPos: any;
    return new Promise(resolve => {
      navigator.geolocation.getCurrentPosition(position => {
        currentPos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
        resolve(currentPos);
      },
      error => {
        console.log(error);
      });
    });
  }
}
